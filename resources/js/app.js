require('./bootstrap');
window.Vue = require('vue');

import Swal from 'sweetalert2'
import EmployeeExpense from './components/EmployeeExpense.vue'
import Attendance from './components/Attendance.vue'
import VueProgressBar from 'vue-progressbar'
import moment from "moment"
import momentTimezone from "moment-timezone"


window.Swal = Swal;
window.moment = moment;


import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

const options = {
  color: '#fff',
  failedColor: '#874b4b',
  thickness: '10px',
  transition: {
    speed: '0.2s',
    opacity: '0.9s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}
const toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
window.toast = toast;

Vue.use(VueProgressBar, options)
Vue.filter('myDate', (date) => {
    return moment(date).format('MMMM Do, YYYY');
});

Vue.filter('myTime', (date) => {
    return moment.tz(date,'Asia/Dhaka').format('h:mm a');
});


const app = new Vue({
    el: '.wrapper',
    components: {
        'employee-expense' : EmployeeExpense,
        'attendance' : Attendance
    },
    methods: {
        deleteEmployee(id) {
            Swal({
                title: 'Are you sure to delete employee?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    axios.delete(`/employee/${id}`)
                        .then(({ data }) => {
                            Swal(
                                'Deleted!',
                                data,
                                'success'
                            );
                            setTimeout(() => location.reload(), 2000);
                        })
                }
            })
        },
        deleteProduct(id) {
            Swal({
                title: 'Are you sure to delete this product?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    axios.delete(`/product/${id}`)
                        .then(({ data }) => {
                            Swal(
                                'Deleted!',
                                data,
                                'success'
                            );
                            setTimeout(() => location.reload(), 2000);
                        })
                }
            })
        },
        logOut(id) {
            Swal({
                title: 'Are you sure to Sign Out?',
                text: "Be sure!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Sign Out!'
            }).then((result) => {
                if (result.value) {
                    axios.post('/logout')
                        .then(response => {
                            Swal(
                                'Signed Out!',
                                'Please Login to Start again.',
                                'success'
                            );
                            setTimeout(() => location.reload(), 3000);
                            
                        })
                }
            })
        }
    }
});
