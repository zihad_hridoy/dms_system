@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">User</h3>
        <a href="{{route('user.add')}}" class="text-right btn btn-success"><i class="fa fa-users fa-fw"></i> Add User</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Mobile</th>
                                <th>Gender</th>
                                <th>Designation</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                                <td>{{ $user->mobile }}</td>
                                <td>{{ $user->gender}}</td>
                                <td>{{ $user->designation }}</td>
                                <td>{{ $user->address}}</td>
                                <td>
                                    
                                    <a href="{{route('user.edit',$user->id)}}" class="btn btn-warning">
                                        <i class="fa fa-edit"></i> 
                                    </a>
                                    <a href="{{route('user.delete',$user->id)}}"class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?');" > 


                                        <i class="fa fa-trash"></i> 
                                    </a>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
    <!-- /.box-body -->
</div>
</section>

@endsection