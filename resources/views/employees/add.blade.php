@extends('layouts.master')
@section('page_main_content')

@if(session('err'))
        <div class="alert alert-danger alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i>Error Alert!</h4>
            {{ session('err') }}
        </div>
@endif
<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Add new Employee</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ route('employee.store') }}">
				@csrf
				<div class="box-body">
					<div class="form-group col-sm-3 {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Name">
						@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group col-sm-3 {{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" name="email" class="form-control" placeholder="Enter email" value="{{ old('email') }}">
						@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group col-sm-3 {{ $errors->has('phone') ? ' has-error' : '' }}">
						<label for="phone">Phone Number</label>
						<input type="text" name="phone" class="form-control" placeholder="Enter Phone Number" value="{{ old('phone') }}">
						@if ($errors->has('phone'))
						<span class="help-block">
							<strong>{{ $errors->first('phone') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group col-sm-3 {{ $errors->has('gender') ? ' has-error' : '' }}">
						<label>Gender: </label><br>
						<label>
							<input type="radio" value="m" name="gender" {{ old('gender') == 'm' ? 'checked': '' }}> Male
						</label>
						<label>
							<input type="radio" value="f" name="gender" {{ old('gender') == 'f' ? 'checked': '' }}> Female
						</label>
						@if ($errors->has('gender'))
						<span class="help-block">
							<strong>{{ $errors->first('gender') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group col-sm-3 {{ $errors->has('dob') ? ' has-error' : '' }} clearfix">
						<label class="control-label">Date of Birth</label>
						<div class="input-group col-md-12">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" name="dob" class="form-control input-sm datepicker" value="{{old('dob')}}" placeholder="Date of Birth">
						</div>
						@if ($errors->has('dob'))
						<span class="help-block">
							<strong>{{ $errors->first('dob') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('present_addr') ? ' has-error' : '' }}">
                        <label>Present Address</label>
                        <textarea name="present_addr" class="form-control input-sm" rows="3">
                        	{{ trim(old('present_addr')) }}
                        </textarea> 
                        @if ($errors->has('present_addr'))
						<span class="help-block">
							<strong>{{ $errors->first('present_addr') }}</strong>
						</span>
						@endif
                    </div>

                    <div class="form-group col-sm-3 {{ $errors->has('permanent_addr') ? ' has-error' : '' }}">
                        <label>Permanent Address</label>
                        <textarea name="permanent_addr" class="form-control input-sm" rows="3">
                        	{{ trim(old('permanent_addr')) }}
                        </textarea>
                        @if ($errors->has('permanent_addr'))
						<span class="help-block">
							<strong>{{ $errors->first('permanent_addr') }}</strong>
						</span>
						@endif
                    </div>
					
					
					<div class="form-group col-sm-3">
						<label>Father Name</label>
						<input type="text" class="form-control" name="father_name" placeholder="Father Name" value="{{ old('father_name') }}">
					</div>


					<div class="form-group col-sm-3 {{ $errors->has('department') ? ' has-error' : '' }} clearfix">
						<label for="Department">Department</label>
						<select name="department" class="form-control">
							<option value="">Select</option>
							<option value="Sales" {{ old('department') == 'Sales' ? 'selected': '' }}>Sales</option>
							<option value="IT" {{ old('department') == 'IT' ? 'selected': '' }}>IT</option>
							<option value="Stock" {{ old('department') == 'Stock' ? 'selected': '' }}>Stock</option>
							<option value="Accounts" {{ old('department') == 'Accounts' ? 'selected': '' }}>Accounts</option>
						</select>
						@if ($errors->has('department'))
						<span class="help-block">
							<strong>{{ $errors->first('department') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('designation') ? ' has-error' : '' }}">
						<label for="designation">Designation</label>
						<select name="designation" class="form-control">
							<option value="">Select</option>
							<option value="CEO" {{ old('designation') == 'CEO' ? 'selected': '' }}>CEO</option>
							<option value="Manager" {{ old('designation') == 'Manager' ? 'selected': '' }}>Manager</option>
							<option value="CFO" {{ old('designation') == 'CFO' ? 'selected': '' }}>CFO</option>
							<option value="Employee" {{ old('designation') == 'Employee' ? 'selected': '' }}>Employee</option>
						</select>
						@if ($errors->has('designation'))
						<span class="help-block">
							<strong>{{ $errors->first('designation') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('joining_salary') ? ' has-error' : '' }}">
						<label for="joining_salary">Joining Salary</label>
						<input type="text" name="joining_salary" class="form-control" placeholder="Enter Joining salary" value="{{ old('joining_salary') }}">
						@if ($errors->has('joining_salary'))
						<span class="help-block">
							<strong>{{ $errors->first('joining_salary') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('salary') ? ' has-error' : '' }}">
						<label for="salary">Salary</label>
						<input type="text" name="salary" class="form-control" placeholder="Enter salary" value="{{ old('salary') }}">
						@if ($errors->has('salary'))
						<span class="help-block">
							<strong>{{ $errors->first('salary') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('join_date') ? ' has-error' : '' }}">
						<label class="control-label">Joining Date</label>
						<div class="input-group col-md-12">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" name="join_date" class="form-control input-sm datepicker" value="{{old('join_date')}}" placeholder="Date of Birth">
						</div>
						@if ($errors->has('join_date'))
						<span class="help-block">
							<strong>{{ $errors->first('join_date') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('photo') ? ' has-error' : '' }}">
						<label for="exampleInputFile">Profile Photo</label>
						<input type="file" id="exampleInputFile" name="photo">
						@if ($errors->has('photo'))
						<span class="help-block">
							<strong>{{ $errors->first('photo') }}</strong>
						</span>
						@endif
					</div>
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary btn-lg">Submit</button>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection