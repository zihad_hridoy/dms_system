@extends('layouts.master')
@section('style')
<style type="text/css">

</style>
@endsection
@section('page_main_content')
<div class="box">
    <div class="box-header">
        <h1 class="box-title">Take Attendance of today <strong> ({{ date("F j, Y") }})</strong></h1>
        <a href="{{ route('attendance.index') }}" class="text-right btn btn-info pull-right"><i class="fa fa-book fa-fw"></i> View Attendance List</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<attendance></attendance>
</div>
@endsection