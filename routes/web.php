<?php


use App\User;

Auth::routes();
Route::get('/', 'HomeController@index')->name('dashboard');

// user
Route::get('/user','UserController@index');
Route::get('/user/add',['as'=>'user.add','uses'=>'UserController@create']);
Route::post('/user/create',['as'=>'user.create','uses'=>'UserController@store']);
Route::get('/user/delete/{id}',['as'=>'user.delete','uses'=>'UserController@destroy']);
Route::get('/user/edit/{id}',['as'=>'user.edit','uses'=>'UserController@edit']);
Route::put('/user/update/{id}',['as'=>'user.update','uses'=>'UserController@update']);
//end user





// Resourceful Route
Route::resource('employee' ,'EmployeeController');
Route::resource('employee-expense' ,'EmployeeExpenseController');
Route::resource('attendance' ,'AttendanceController');


// Routes for Ajax Call
Route::get('all_employee','EmployeeController@AllEmployee');
Route::get('all_employee_expense','EmployeeExpenseController@AllEmployeeExpense');
Route::get('all_attendance','AttendanceController@AllAttendance');

//Products Route
Route::resource('product', 'ProductController');
//Sales Route
Route::resource('sale', 'SalesController');
Route::post('/import', 'SalesController@import');




