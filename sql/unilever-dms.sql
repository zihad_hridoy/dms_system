-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2019 at 03:50 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unilever-dms`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `absent_date` date NOT NULL,
  `note` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `absent_date`, `note`, `created_at`, `updated_at`) VALUES
(3, 1, '2019-01-05', NULL, '2019-01-04 20:47:33', '2019-01-04 20:47:33'),
(4, 2, '2019-01-05', NULL, '2019-01-04 20:47:33', '2019-01-04 20:47:33'),
(5, 10, '2019-01-05', NULL, '2019-01-04 20:47:33', '2019-01-04 20:47:33'),
(6, 11, '2019-01-05', NULL, '2019-01-04 20:47:33', '2019-01-04 20:47:33');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `DOB` date NOT NULL,
  `present_addr` text NOT NULL,
  `permanent_addr` text NOT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `department` varchar(30) NOT NULL,
  `designation` varchar(30) NOT NULL,
  `join_date` date NOT NULL,
  `joining_salary` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `phone`, `gender`, `DOB`, `present_addr`, `permanent_addr`, `father_name`, `department`, `designation`, `join_date`, `joining_salary`, `salary`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Rafid', 'princerafid01@gmail.com', '01967890878', 'm', '2018-07-04', 'fnv fb vhbf vbhf', 'bvfhv hbf vbhf vhgb', NULL, 'Sales', 'CEO', '2018-12-18', 1000000, 120000000, 'ZA99EFutw3hVlylvp1tq78oGAmeqnxgPqaMVGgCg.jpeg', '2018-12-25 14:55:22', '2018-12-25 06:46:26'),
(2, 'sahil', 'rafid@gmail.com', '01967890870', 'f', '2018-10-03', 'ssssssss', 'ssssssss', NULL, 'Sales', 'CEO', '2018-12-18', 1000000, 120000000, 'pJObAqQYB2FRAPp2GlChmjocq5If2NvSpwofGgzj.jpeg', '2018-12-25 15:21:50', '2018-12-25 09:21:50'),
(7, 'Rafid Hasan', 'admin@example.cm', '01955555555', 'm', '2018-07-04', 'lll', 'kkkk', NULL, 'Sales', 'CEO', '2018-12-18', 1000000, 120000000, 'g9eRyNpAeYl7RwzLM3p345UMoCB0OvNGW0wOuIFB.jpeg', '2018-12-25 15:27:38', '2018-12-25 09:27:38'),
(8, 'Rafid', 'amin@example.cm', '01955555585', 'm', '2018-07-04', 'lll', 'kkkk', NULL, 'Sales', 'CEO', '2018-12-18', 1000000, 120000000, NULL, '2018-12-25 11:03:14', '2018-12-24 20:47:36'),
(9, 'Abir', 'abir@gmail.com', '019678908756', 'm', '1990-02-01', 'dhaka', 'Bangladesh', NULL, 'Sales', 'Manager', '2018-12-31', 1000000, 120000000, 'BLS5TMBtky9A8USQvQAYuxBIFhUiCFjPdjYLOlOA.jpeg', '2018-12-25 14:59:08', '2018-12-24 22:03:50'),
(10, 'Rehana Akhtar', 'sahil@gmail.com', '019451213333', 'f', '2018-07-04', 'gtght', 'thtyhyhy', 'Abdul Kadir', 'Stock', 'CEO', '2018-12-18', 1000000, 120000000, 'x9NHiedSKEhLwQolGCoScfbrbFsZmmJs4YZM2jzy.jpeg', '2018-12-25 14:59:13', '2018-12-25 06:42:35'),
(11, 'Opey Hasan', 'abir@gmail.me', '01967890855', 'm', '2018-07-03', 'Mathpara', 'Bangladesh', NULL, 'IT', 'Manager', '2018-12-18', 1000000, 120000000, 'gDulxm5IqsG7D8YxRPa1nYMCkzqQoSNlpanx75Xy.jpeg', '2018-12-25 14:59:17', '2018-12-24 22:47:40'),
(12, 'Opey Hasan', 'abir@gmail.meo', '01967890844', 'm', '2018-07-03', 'Mathpara', 'Bangladesh', NULL, 'IT', 'Manager', '2018-12-18', 1000000, 120000000, 'JFMQzcurF4S4ZELmZSyzYQ9uVoS2nbI5SMHVY0BK.jpeg', '2018-12-25 14:59:21', '2018-12-24 22:48:45'),
(13, 'Opey Hasan', 'abir@gmail.m', '01967890851', 'm', '2018-07-03', 'Mathpara', 'Bangladesh', NULL, 'IT', 'Manager', '2018-12-18', 1000000, 120000000, 'ZjUikOYDLqKLLaTgjZJzJCVXHWLa5GI2vDdzXksi.jpeg', '2018-12-25 14:59:24', '2018-12-24 22:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `employee_expenses`
--

CREATE TABLE `employee_expenses` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `expenses_type` varchar(100) NOT NULL,
  `expenses_amount` int(11) NOT NULL,
  `expenses_details` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_expenses`
--

INSERT INTO `employee_expenses` (`id`, `employee_id`, `expenses_type`, `expenses_amount`, `expenses_details`, `created_at`, `updated_at`) VALUES
(1, 1, 'Salary', 50000, 'wow!', '2018-12-06 05:00:33', '2018-12-26 05:00:33'),
(2, 2, 'Salary', 50000, 'wow!', '2018-12-26 05:01:20', '2018-12-26 05:01:20'),
(3, 7, 'Provident Fund', 1200, 'woow!', '2018-12-26 05:02:24', '2018-12-26 05:02:24'),
(4, 1, 'Salary', 12000, 'add.', '2018-12-26 05:05:04', '2018-12-26 05:05:04'),
(5, 10, 'TA-DA', 1200, 'Transportation cost', '2018-12-26 12:25:43', '2018-12-26 12:25:43'),
(6, 12, 'Provident Fund', 120, 'He needed that', '2018-12-26 12:34:32', '2018-12-26 12:34:32'),
(7, 9, 'Salary', 20000, 'salary', '2018-12-26 12:35:35', '2018-12-26 12:35:35'),
(8, 2, 'Provident Fund', 10, NULL, '2018-12-26 12:35:48', '2018-12-26 12:35:48'),
(9, 8, 'Provident Fund', 5000, NULL, '2018-12-26 12:40:57', '2018-12-26 12:40:57'),
(10, 13, 'Salary', 1000, 'ki', '2018-12-26 12:55:57', '2018-12-26 12:55:57'),
(11, 7, 'Provident Fund', 1000, 'xyz....', '2019-01-02 11:04:22', '2019-01-02 11:04:22'),
(12, 7, 'TA-DA', 1500, 'asdfghjkl', '2019-01-02 11:10:24', '2019-01-02 11:10:24'),
(13, 9, 'Powder', 1000, 'Expense Detail', '2019-01-03 12:28:53', '2019-01-03 12:28:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_17_230433_create_employees_table', 2),
(4, '2018_12_17_235018_create_employees_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_details` text NOT NULL,
  `product_price` double NOT NULL,
  `product_sku_code` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_details`, `product_price`, `product_sku_code`, `created_at`, `updated_at`) VALUES
(3, 'meril powder', '250gm', 25, 48789, '2019-01-03 16:34:43', '2019-01-03 16:34:43'),
(5, 'lip jel', '10g', 7.25, 5969885, '2019-01-03 22:57:04', '2019-01-03 16:57:04'),
(6, 'mum waters', '5 ltr', 70, 1221, '2019-01-03 22:52:43', '2019-01-03 16:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `remember_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `mobile`, `gender`, `designation`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Rafid', 'admin@example.com', '$2y$10$zlKJE/jcyO9uYtPfZpTbA.h15HQih4PK0mUv9tt25reXmv8bD1hx6', 'Super Admin', '01906489959', 'm', 'Super Admin', '', 'h6MfVX7zIBqxTGcOTIY6xOOstyqlsm2ePP1MbRAMPR7sMM72Vy5kESXzsZUO', '2019-01-05 02:26:30', '0000-00-00 00:00:00'),
(2, 'Mahmud Hasan', 'princerafid01@gmail.com', '$2y$10$QVAvJgFMMG7prSpsnnMcq.g.P57YxccSHuFVCrjZ62AQrpLBwvoK.', 'Super Admin', '01967890878', 'm', 'IT', 'Dhaka', NULL, '2019-01-02 21:23:15', '2019-01-02 15:23:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_expenses`
--
ALTER TABLE `employee_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `employee_expenses`
--
ALTER TABLE `employee_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
