<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];

    public function employee_expense()
    {
        return $this->hasOne('App\EmployeeExpense');
    }
    public function attendance()
    {
        return $this->hasMany('App\Attendance');
    }
    public function count_absent()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->groupBy('employee_id');
    }
}
