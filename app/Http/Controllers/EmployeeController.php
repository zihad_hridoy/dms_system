<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function AllEmployee()
    {
        $employees = Employee::all();
        return response()->json($employees);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employees'] = Employee::all();
        return view('employees.employee-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'name' => 'string|required|max:255',
            'email' => 'required|email|unique:employees',
            'phone' => 'required|regex:/(01)[0-9]{9}/|unique:employees',
            'gender' => 'required',
            'dob' => 'required|date',
            'present_addr' => 'required',
            'permanent_addr' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'join_date' => 'required',
            'joining_salary' => 'required',
            'salary' => 'required',
            'photo' => 'sometimes|image',
            'father_name' => 'sometimes|string',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('err', 'Please check the highlighted inputs!');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('app');
        }


        $employee = Employee::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'dob' => date('Y-m-d', strtotime($request->dob)),
            'present_addr' => $request->present_addr,
            'permanent_addr' => $request->permanent_addr,
            'department' => $request->department,
            'designation' => $request->designation,
            'join_date' => date('Y-m-d', strtotime($request->join_date)),
            'joining_salary' => $request->joining_salary,
            'salary' => $request->salary,
            'father_name' => $request->father_name,
            'photo' => $photo ?? null
        ]);
        if ($employee) {
            $request->session()->flash('msg', 'Employee Created Successfully!');
            return redirect()->route('employee.index');
        } else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $id = $employee;
        $data['employee'] = Employee::findOrFail($id)->first();
        return view('employees.profile',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $id = $employee;
        $employee = Employee::findOrFail($id)->first();
        if($employee){
            return view('employees.edit')->with('employee' , $employee);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $id = $employee;
        $f_employee = Employee::findOrFail($id)->first();

        $validator = Validator::make($request->all() ,[
            'name' => 'string|required|max:255',
            'email' => 'required|email|unique:employees,email,'.$f_employee->id,
            'phone' => 'required|regex:/(01)[0-9]{9}/|unique:employees,phone,'.$f_employee->id,
            'gender' => 'required',
            'dob' => 'required|date',
            'present_addr' => 'required',
            'permanent_addr' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'join_date' => 'required',
            'joining_salary' => 'required',
            'salary' => 'required',
            'photo' => 'sometimes|image'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('err', 'Please check the highlighted inputs!');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public');
            $photo = substr($photo, 7);
        } else{
            $photo = $f_employee->photo;
        }


        $employee = $f_employee->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'dob' => date('Y-m-d', strtotime($request->dob)),
            'present_addr' => $request->present_addr,
            'permanent_addr' => $request->permanent_addr,
            'department' => $request->department,
            'designation' => $request->designation,
            'join_date' => date('Y-m-d', strtotime($request->join_date)),
            'joining_salary' => $request->joining_salary,
            'salary' => $request->salary,
            'father_name' => $request->father_name,
            'photo' => $photo ?? null
        ]);

        if ($employee) {
            $request->session()->flash('msg', 'Employee\'s Information edited Successfully!');
            return redirect()->route('employee.index');
        } else{
            return redirect()->back();
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee = Employee::find($employee)->first();
        $employee->delete();
        return "User deleted Successfully!";
    }
}
