<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function AllAttendance()
    {
        $attendances = Attendance::all();
        return $attendances;   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employees'] = Employee::with('attendance')->has('attendance')->get();
        return view('employees.attendance-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['employees'] = Employee::all();
        return view('employees.take-attendance' ,$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->all())){
            $exist_attendances = Attendance::where('absent_date' ,Carbon::now()->toDateString())->get();
                if ($exist_attendances) {
                    foreach ($exist_attendances as $exist) {
                        $exist->delete();
                    }
                }
        }else{
            foreach ($request->all() as $id) {
            $exist_attendances = Attendance::where('absent_date' ,Carbon::now()->toDateString())->get();
                if ($exist_attendances) {
                    foreach ($exist_attendances as $exist) {
                        $exist->delete();
                    }
                }
            }
        }
        foreach ($request->all() as $id) {
            
            $attendances = Attendance::create([
                'employee_id' => $id,
                'absent_date' => Carbon::now()->toDateString()
            ]);
        }

        if ($attendances) {
            return ['Attendance Taken.',200];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
}
