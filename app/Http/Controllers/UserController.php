<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

       $user=User::all();
       return view('user.user_list')->with('user',$user);

    }

    public function create(){

    	return view('user.add');
    }

    public function store(Request $request){

    	$request->validate([
       'name'=>'required',
       'email'=>'required|unique:users|email',
       'password'=>'required',
       'role'=>'required',
       'mobile'=>'required|regex:/(01)[0-9]{9}/',
       'gender'=>'required',
       'designation'=>'required',
       'address'=>'required',
    	]);
      
       $user=new User();
       $user->name=$request->name;
       $user->email=$request->email;
       $user->password=bcrypt($request->password);
       $user->role=$request->role;
       $user->mobile=$request->mobile;
       $user->gender=$request->gender;
       $user->designation=$request->designation;
       $user->address=$request->address;

       $user->save();

       return redirect()->back()->with('msg','New User Successfully created');


    }
    public function edit($id){
      $user=User::findOrFail($id);
      return view('user.edit')->with('user',$user);

    }
    public function update(Request $request, $id){
       
        $user=User::find($id);

       $request->validate([
       'name'=>'required',
       'email'=>'required|email',
       'password'=>'required',
       'role'=>'required',
       'mobile'=>'required|regex:/(01)[0-9]{9}/',
       'gender'=>'required',
       'designation'=>'required',
       'address'=>'required',
      ]);

       
       $user->name=$request->name;
       $user->email=$request->email;
       $user->password=bcrypt($request->password);
       $user->role=$request->role;
       $user->mobile=$request->mobile;
       $user->gender=$request->gender;
       $user->designation=$request->designation;
       $user->address=$request->address;

       $user->save();

       return redirect('/user')->with('msg','User Update Successfully');


    }

    public function destroy($id){

          User::find($id)->delete();
          return redirect()->back()->with('msg','Successfully Deleted');
    }
	
    }

    