<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\SalesImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Sale;

class SalesController extends Controller
{	
	public function index(){
		return view('sales.index');
	}

	public function create(){
		return view('sales.add');
	}

	//imports excel 
    public function import(){
        $array = (new SalesImport)->toArray(request()->file('excel_file'));
      
        for ($i = 12; $i < count($array[0]); $i++) {
        	if ($array[0][$i][1] !== "Total") {
        		$sale = new Sale();
        		$sale->sku_code = $array[0][$i][1];
        		$sale->sku = $array[0][$i][2];
        		$sale->pack_size = $array[0][$i][5];
        		$sale->ctn_issue = $array[0][$i][7];
        		$sale->unit_issue = $array[0][$i][8];
        		$sale->ctn_sales = $array[0][$i][9];
        		$sale->unit_sales = $array[0][$i][11];
        		$sale->ctn_free = $array[0][$i][14];
        		$sale->unit_free = $array[0][$i][16];
        		$sale->ctn_returned = $array[0][$i][17];
        		$sale->unit_returned = $array[0][$i][18];
        		$sale->ctn_srt_excess = $array[0][$i][20];
        		$sale->unit_srt_excess = $array[0][$i][21];
        		$sale->sales_value = $array[0][$i][23];
        		$sale->save();
        	}

        	else{
        		session()->flash('msg', 'Sales Excel Imported Successfully!');
        		return redirect('/sale');
        	}
        }
    }
}
