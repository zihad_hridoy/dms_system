<?php

namespace App\Http\Controllers;

use App\EmployeeExpense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function AllEmployeeExpense()
    {
        $e_expense = EmployeeExpense::with('employee')->orderBy('id', 'desc')->get();
        return $e_expense->toJson();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.employee-expense');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request ,[
            'employee_id' => 'numeric|required',
            'expenses_type' => 'string|required',
            'expenses_amount' => 'numeric|required',
            'expenses_details' => 'sometimes'
        ]);
        $e_expense = EmployeeExpense::create($request->all());
        if ($e_expense) {
            return response()->json('Expense Added!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeExpense  $employeeExpense
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeExpense $employeeExpense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeExpense  $employeeExpense
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeExpense $employeeExpense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeExpense  $employeeExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeExpense $employeeExpense)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeExpense  $employeeExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeExpense $employeeExpense)
    {
        //
    }
}
